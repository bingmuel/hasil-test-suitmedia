const itemsPerPageOptions = [10, 20, 50];
let currentPage = 1;
let itemsPerPage = itemsPerPageOptions[0];
let sortBy = 'published_at';
let sortOrder = 'asc';

function renderPosts(data) {
  const postContainer = document.getElementById('post-container');
  postContainer.innerHTML = '';

  const startIndex = (currentPage - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;

  const paginatedPosts = data.slice(startIndex, endIndex);

  paginatedPosts.forEach(post => {
    const postCard = document.createElement('div');
    postCard.classList.add('post-card');

    const thumbnail = document.createElement('img');
    thumbnail.classList.add('post-thumbnail');
    thumbnail.src = post.small_image;
    thumbnail.alt = post.title;

    const postDetails = document.createElement('div');
    postDetails.classList.add('post-details');

    const title = document.createElement('h2');
    title.textContent = post.title;

    postDetails.appendChild(title);

    postCard.appendChild(thumbnail);
    postCard.appendChild(postDetails);

    postContainer.appendChild(postCard);
  });

  renderPagination(data.length);
}

function sortPosts() {
  sortBy = document.getElementById('sort').value;
  fetchPosts();
}

function changePerPage() {
  itemsPerPage = parseInt(document.getElementById('perPage').value);
  currentPage = 1;
  fetchPosts();
}

function renderPagination(totalItems) {
  const totalPages = Math.ceil(totalItems / itemsPerPage);
  const paginationContainer = document.getElementById('pagination');
  paginationContainer.innerHTML = '';

  for (let i = 1; i <= totalPages; i++) {
    const link = document.createElement('span');
    link.classList.add('pagination-link');
    link.textContent = i;

    if (i === currentPage) {
      link.classList.add('active');
    }

    link.addEventListener('click', () => {
      currentPage = i;
      fetchPosts();
    });

    paginationContainer.appendChild(link);
  }
}

const apiUrl = 'https://suitmedia-backend.suitdev.com/api/ideas';

async function fetchPosts() {
  try {
    const proxyUrl = 'https://cors-anywhere.herokuapp.com/';
    const response = await fetch(`${proxyUrl}${apiUrl}?page[number]=${currentPage}&page[size]=${itemsPerPage}&append[]=small_image&sort=${sortBy}${sortOrder === 'desc' ? '&sort_order=desc' : ''}`);
    const data = await response.json();

    renderPosts(data.data);
  } catch (error) {
    console.error('Error fetching data:', error);
  }
}

// Initial fetch
fetchPosts();

